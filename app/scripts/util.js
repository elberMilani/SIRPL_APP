$(document).ready(function() {
	
	$("#item2").hide();
	$("#item3").hide();

	$("#incluirEstabelecimento").click(function(){
        $("#panel1").trigger('click');
        $("#item2").show();
        $("#incluirEstabelecimento span").html('Excluir Estabelecimento<i class="fa fa-minus-circle" style="margin-left: 5px;"></i>');
        //$("#testeItem1").toggleClass("fa fa-minus-circle");
        //$("#panel2").trigger('click');
        //$('#valor').text('R$ '+value);
    }); 
 
    $('.dataTable').DataTable();
    $(".validate").validate();


    $(".data").mask("99/99/9999");
    $(".telefone-fixo").mask("(99) 9999-9999");
    $(".telefone-celular").mask("(99) 9999-99999");
    $(".telefone").mask("(99) 9999-9999?");
    $(".cpf").mask("999.999.999-99");
    $(".cnpj").mask("99.999.999/9999-99");
    $(".cei").mask("99.99999.9-99");
    $(".matricula-caixa").mask("a999999");
    $(".pct").mask("99%");

    $(".data").datepicker();
    
    $("input").blur(function() {
        $("#info").html("Unmasked value: " + $(this).mask());
    }).dblclick(function() {
        $(this).unmask();
    });



 	 // validate signup form on keyup and submit
	$("#form").validate({
		rules: {
			firstname: "required",
			lastname: "required",
			username: {
				required: true,
				email: true
			},
			password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			data: {
				date: true
			},

			
		}
	});


	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	  $('[data-toggle="modal"][title="Excluir"]').tooltip();
	})



	// MENU
	$(".mega-menu").click(function(){
		var display =  $("#" + this.id + "-aba").css("display");

		if (display == "none") {

			$(".box-cell").find("div[id*='-aba']").each(function(){
				
				if ($("#" + this.id).css("display") == "block") {
					$("#" + this.id).hide();
				}

			});
			
			$("#" + this.id + "-aba").toggle();
		} else {
			$(".mega-menu-base").hide();
		}

    });
    $("body").click(function(e){
		if(e.target.offsetParent.className!="mega-menu waves-effect" && e.target.offsetParent.className!="col-md-4") {
			$(".mega-menu-base").hide();
			$(".nav li").removeClass("active");
		} 
	});
	// FIM MENU
	
	
	// 	COLLAPSE COMPONENTE
		$("button#collapseB").click(function(){
				$("button#collapseB>i").toggleClass('fa-chevron-up');
				alert("teste");
    }); 
				
//				$('.collapse')
//         .on('shown.bs.collapse', function() {
//					 	$('#collapseB i').find(".fa-chevron-down")
//                 .removeClass("fa-chevron-down")
//                 .addClass("fa-chevron-up");
//             })
//         .on('hidden.bs.collapse', function() {
//             $(this)
//                 .parents('tr')
//                 .find(".fa-chevron-up")
//                 .removeClass("fa-chevron-up")
//                 .addClass("fa-chevron-down");
//          });

	// 	FIM COLLAPSE COMPONENTE

});


if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){ 
 var ffversion=new Number(RegExp.$1) 
 if (ffversion<=30){
 	$(".md-check").removeClass("md-check");
 	$(".md-switch").removeClass("md-switch");
	}
}





