'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider
  .when("/index", {
    templateUrl : "partials/index.html"
  })
  .when("/insumos", {
    templateUrl : "partials/insumoPage.html"
  })
  .when("/proposituras", {
    templateUrl : "partials/propositurasPage.html"
  });
  $routeProvider.otherwise({redirectTo: '/partials/index.html'});
}]);

/* import axios from "axios"; */

/* 
export default {
  name: "delete",
  data() {
    return {
      insumo:Object
    };
  },
  created() {
    this.getById();
    },
  methods: {
      getById() {
      axios
        .get("http://localhost:3000/insumos/getById/"+this.$route.params.insumo_id)
        .then(response => {
          this.insumo = response.data[0];
        });
    },
    excluir() {
      axios
        .delete("http://localhost:3000/insumos/delete/"+this.$route.params.insumo_id)
        .then(response => {     console.log(res)  
        });
    }
  },
  mounted: function() {
    this.getById();
  }
}; */