angular.module("myApp", ["ui.router"])
/* angular.module('sifugApp', [
    'ngRoute'
  ]) */
  .run(function () {
  })
  .config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('index', {
            url: '/index',
            templateUrl: 'partials/index.html'
        })
        .state('insumos', {
            templateUrl: 'partials/insumos.html'      
        })
        .state('proposituras', {
            templateUrl: 'partials/proposituras.html'      
        });

});

/*   .config(function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('');
    $routeProvider
    .when("/index", {
        template : "partials/index.html"
    })
    .when('/insumos',{
        template : "partials/insumoPage.html"
    })
    .when('/proposituras',{
        template : "partials/propositurasPage.html"
    })
    .otherwise({
        template : "partials/index.html"
    });
  }); */