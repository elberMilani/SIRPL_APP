var myApp = angular.module('myApp', []);
var enabled;
var titulo;
var visible;
var visibleGrid = false;
var temas;
var subtemas;
var msgVisible = true;
var labelPalavraChave;




myApp.controller("tema-controller", function ($scope, $http) {
  $http.get('http://localhost:3000/v1/temas').then(function (result) {
    console.log(result.data['data'])
    $scope.temas = result.data['data'];
    $scope.subtemas = null;

  },(err)=>{
    console.error(err)
  }
  );

  $http.get('http://localhost:3000/v1/tipos-insumo').then(function (result) {
    console.log(result.data['data'])
    $scope.tipoInsumo = result.data['data'];

  },
  );

  $http.get('http://localhost:3000/v1/palavraschave/').then(function (result) {
    console.log(result.data['data'])
    $scope.palavraschave = result.data['data'];
  
  },
  );


  $scope.getByTema = function (id) {
    $http.get('http://localhost:3000/v1/insumos/getByTema/' + id).then(function (result) {
      $scope.insumos = result.data['data'];
      $scope.visibleGrid = true;
      console.info(result);
    })
  };

  $scope.deleteById = function (id) {
    $http.delete('http://localhost:3000/v1/insumos/delete/' + id).then(function (result) {
      console.info(result);
    })
  };


  $scope.Save = function () {
    var objInsumo = {
      idtipoinsumo: $scope.insumo.selectedTipoInsumo,
      idtema: $scope.insumo.selectTemaModal,
        //idsubtema: $scope.insumo.selectSubTemaModal,
      idsubtema: 2,
      titulo: $scope.insumo.titulo,
      descricao: $scope.insumo.descricao,
      //palavraschave:  $scope.insumo.palavraschave,
      palavraschave: 2,
      endereco: $scope.insumo.endereco,
      ativo: 1,
      documento: null,
      idinsumo: 0,
      versionar: $scope.insumo.versionar,
      dtinsumo: '01/01/2018'
    };
    res = $http.post('http://localhost:3000/v1/insumos/insert', objInsumo).then(function (response) {
      $scope.insumo.idInsumo = response.data.ID_INSUMO_OUT;
 
    });
 
    if ($scope.insumo.idInsumo != null) {
      // Salvar palavras-chave
      angular.forEach($scope.insumo.palavraschave, function(value, index) {
         
        var objPalavraChave = {
          idinsumo: $scope.insumo.idInsumo,
          idpalavrachave:value
        }
        
        $http.post('http://localhost:3000/v1/palavraschave/insert', objPalavraChave);
      });
    
      // Salvar documento
      var objPalavraChave = {
        idinsumo: $scope.insumo.idInsumo,
        documento:null
      }
        $http.post('http://localhost:3000/v1/insumo-documentos/insert', objPalavraChave);
      
    }

    res.success(function (data, status, headers, config) {
     

    });
    res.error(function (data, status, headers, config) {
      alert("failure message: " + JSON.stringify({ data: data }));
    });
  };

 
  $scope.Update = function () {
    return $http({
      method: "POST",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      url: 'http://localhost:3000/v1/insumos/update',
      data: $.param({
        "IdTipoInsumo": $scope.insumoAlteracao.sInsumo,
        "NomeInsumo": $scope.insumoAlteracao.titulo,
        "Link": $scope.insumoAlteracao.endereco,
        "Tags": $scope.insumoAlteracao.palavrachave,
        "Descricao": $scope.insumoAlteracao.descricao,
        "Ativo": 1, //Ajustar o parâmero imagem
        "IdTema": $scope.insumoAlteracao.selectedTemaModal,
        "IdInsumo": $scope.insumoAlteracao.idInsumo
      })
    });
  }



  $scope.getById = function (id, action) {
    if (id != null) {
      $http.get('http://localhost:3000/v1/insumos/getById/' + id).then(function (data) {

        if (action == 1) {
          $scope.enabled = true;
          $scope.titulo = 'Visualizar Insumos';
          $scope.visible = false;
        }
        else {
          $scope.enabled = false;
          $scope.titulo = 'Editar Insumos';
          $scope.visible = true;
        }


        $scope.insumoAlteracao = data.data[0];
        $scope.insumoAlteracao.titulo = $scope.insumoAlteracao.NO_INSUMO;
        $scope.insumoAlteracao.idInsumo = $scope.insumoAlteracao.NU_PRLZ35;
        //$scope.insumoAlteracao.sInsumo = 1;//$scope.insumoAlteracao.NU_TIPO_INSUMO;
        $scope.insumoAlteracao.palavrachave = $scope.insumoAlteracao.NO_PALAVRA;
        $scope.insumoAlteracao.descricao = $scope.insumoAlteracao.DE_INSUMO;
        $scope.insumoAlteracao.selectedTema = $scope.insumoAlteracao.NU_TEMA; 
        $scope.insumoAlteracao.endereco = $scope.insumoAlteracao.ED_INSUMO;
        $scope.insumoAlteracao.versionar = $scope.insumoAlteracao.IC_INSUMO_ATIVO;

        console.info(data);
      })
    }
  };

  $scope.clearForm = function () {
    $scope.insumo = null;
  };



  $scope.setInfoModalDelete = function (id, tituloInsumo) {
    $scope.idInsumo = id;
    $scope.descricao = tituloInsumo;

  };

  $scope.getSubtemaById = function (id) {
    $http.get('http://localhost:3000/v1/subtemas/getSubtemas/' + id).then(function (result) {
      $scope.subtemas = result.data['data'];
      console.info(data);
    })
  };

  function SavePalavraschave(palavraschave,idinsumo){
    angular.forEach(palavraschave, function(value, index) {
      var objPalavraChave = {
        idinsumo: idinsumo,
        palavrachave:value
      }

      $http.post('http://localhost:3000/v1/palavraschave/insert', objPalavraChave);

    });
  };
});